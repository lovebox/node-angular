# Lovebox tech interview repository

Generated with express-generator
https://expressjs.com/en/starter/generator.html

## How to run it

- Clone the repository
- `npm install`
- `npm start`

You can find your server at http://localhost:3000/

## Useful links

For inspiration mainly

[Play store](https://play.google.com/store/apps/details?id=love.lovebox.loveboxapp)
[Website](https://www.lovebox.love)
[Facebook page](https://www.facebook.com/lovelovebox/)
